# Steering Behavior

## Description
Uses AI to perform Seek, Arrive, and Wall Avoidance functionality and abstracts the algorithm's output by filtering it for the Game Engine's Car class.

## Authors and acknowledgment
The Core Game Engine is provided curtosey of Dr. Santiago Ontanon

Seek, Arrive, Wall Avoidance, & Output Filtering Implementation by Christopher S. Good, Jr.
