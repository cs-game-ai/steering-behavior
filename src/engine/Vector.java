package engine;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.Graphics2D;

public class Vector extends GameObject {
    public double m_x;
    public double m_y;
    public GameObject m_car;
    public double m_angle;
    public Color m_color;

    public Vector() {
        m_x = 0;
        m_y = 0;
    }

    public Vector(double x, double y) {
        m_x = x;
        m_y = y;
    }

    public Vector(double x, double y, GameObject subject, double angle, Color color) {
        m_x = x;
        m_y = y;
        m_car = subject;
        m_angle = angle;
        m_color = color;
    }

    // public double getAngle() {
    //     return 
    // }

    public double magnitude() {
        return Math.sqrt(this.m_x * this.m_x + this.m_y * this.m_y);
    }

    public Vector normalize() {
        // double magnitude = this.magnitude();
        // this.m_x /= magnitude;
        // this.m_y /= magnitude;
        return new Vector(this.m_x/this.magnitude(), this.m_y/this.magnitude());
    }

    public double dotprod(Vector vec) {
        return this.m_x * vec.m_x + this.m_y * vec.m_y;
    }

    public Vector add(Vector vec) {
        return new Vector(this.m_x + vec.m_x, this.m_y + this.m_y);
    }

    public Vector subtract(Vector vec) {
        return new Vector(this.m_x - vec.m_x, this.m_y - vec.m_y);
    }

    public Vector scale(double magnitude) {
        return new Vector(this.m_x * magnitude, this.m_y * magnitude);
    }

    static public Vector fromAngle(double angle) {
        return new Vector(Math.cos(angle), Math.sin(angle));
    }

    public void print() {
        System.out.printf("Vector <%f,%f>\n", this.m_x, this.m_y);
    }

    @Override
    public void update(Game game, double delta_t) {
        // TODO Auto-generated method stub
    }

    @Override
    public void draw(Graphics2D g) {
        // TODO Auto-generated method stub
        g.setColor(m_color);
        Car c = (Car) m_car;
        double x = Math.cos(c.getAngle() + m_angle) * 35;
        double y = Math.sin(c.getAngle() + m_angle) * 35;
        g.draw(new Line2D.Double(c.getX(), c.getY(), c.getX() + x, c.getY() + y));
    }

    @Override
    public RotatedRectangle getCollisionBox() {
        // TODO Auto-generated method stub
        return null;
    }
}
