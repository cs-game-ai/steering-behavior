package controllers;

import engine.Car;
import engine.Game;
import engine.GameObject;
import engine.Obstacle;
import engine.RotatedRectangle;
import engine.Vector;

public class WallAvoidanceController extends SeekController {

    private final double DEGREES_90 = Math.PI/2;
    private final double DEGREES_45 = Math.PI/4;

    /** Maximum Ray Cast Distance */
    private final int MAX_LOOK_AHEAD = 64;

    public WallAvoidanceController(GameObject target) {
        super(target);
    }

    /**
     * Create a vector from an angle and provide a scale
     * @param angle Direction of the vector
     * @param scale Magnitude of the vector
     * @return Vector Object (Ray)
     */
    public Vector rayCast(double angle, int scale) {
        return Vector.fromAngle(angle).scale(scale);
    }

    /**
     * Detect when a collision occurrs with an Obstacle
     * @param subject Originating object for ray
     * @param game Game instance to collect Obstacles
     * @param ray Path to follow for collision
     * @return Determination of whether a collision has happened or not
     */
    public boolean collisionDetection(Car subject, Game game, Vector ray) {       
        RotatedRectangle rect = new RotatedRectangle(
            subject.getX() + ray.m_x, 
            subject.getY() + ray.m_y, 
            subject.m_img.getWidth()/2, 
            subject.m_img.getHeight()/2, 
            ray.m_angle);
        /** Iterate through Obstacles to determine if there is a collision */
        for (Obstacle object : game.getObstacles()) {
            if (object.collision(rect)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Calculate the distance between the subject and the collision object
     * @param subject Originating object
     * @param game Game instance
     * @param angle Direction to cast the ray
     * @return Integer value of distance to Obstacle
     */
    public int collisionDistance(Car subject, Game game, double angle) {
        int distance = subject.m_img.getWidth()/2 + 1;
        while (distance < MAX_LOOK_AHEAD) {
            if (collisionDetection(subject, game, rayCast(angle, distance))) {
                return distance;
            }
            distance += 1;
        }
        return distance;
    }

    /**
     * Seek the provided point from the subject vehicle
     * @param subject Seeking Car
     * @param E Point to Seek
     * @return Desired Vector
     */
    public Vector seekPosition(Car subject, Vector E) {
        Vector CarPosition = new Vector(subject.getX(), subject.getY());
        Vector D = E.subtract(CarPosition);
        Vector ND = D.normalize();
        Vector A = ND.scale(MAX_ACCELERATION);
        return A;
    }

    /**
     * Seek and Avoid
     * 
     * Performs the Seek Algorithm while also avoiding Obstacles within the 
     * game instance. The action defaults to the Seek Algorithm, and depending 
     * upon the detection of collision objects, it will modify the A, or desired, 
     * vector to pull the car away from the Obstacle.
     * @param subject Seeking and Avoiding Car
     * @param game Game Instance
     * @return Desired Vector (A) for output filtering
     */
    public Vector avoid(Car subject, Game game, double delta_t) {
        /** Collect distances from obstacles */
        int l = collisionDistance(subject, game, subject.getAngle() - DEGREES_45);
        int f = collisionDistance(subject, game, subject.getAngle());
        int r = collisionDistance(subject, game, subject.getAngle() + DEGREES_45);

        Vector A = seek(subject);

        /** Modify A to handle blocks to the left */
        if (l < MAX_LOOK_AHEAD) {
            A = A.subtract(seekPosition(subject, Vector.fromAngle(subject.getAngle() + DEGREES_90)
                .normalize()
                .scale(l)));
        }
        /** Modify A to handle blocks straight on */
        if (f < MAX_LOOK_AHEAD) {
            A = A.subtract(seekPosition(subject, Vector.fromAngle(subject.getAngle())
                .normalize()
                .scale(f)));
        } 
        /** Modify A to handle blocks to the right */
        if (r < MAX_LOOK_AHEAD) {
            A = A.subtract(seekPosition(subject, Vector.fromAngle(subject.getAngle() - DEGREES_90)
                .normalize()
                .scale(r)));
        }

        /** 
         * Safety Feature that checks if the car may be stuck 
         * (when all collision points are detected) 
         */
        if (l < MAX_LOOK_AHEAD && f < MAX_LOOK_AHEAD && r < MAX_LOOK_AHEAD) {
            A = A.scale(-f);
        }
        
        return A;
    }

    @Override
    public void update(Car subject, Game game, double delta_t, double[] controlVariables) {
        Vector A = avoid(subject, game, delta_t);
        outputFiltering(subject, A, controlVariables);
    }
}
