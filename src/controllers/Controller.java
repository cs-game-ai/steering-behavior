package controllers;

import engine.Car;
import engine.Game;
import engine.Vector;

/**
 *
 * @author santi
 */
public abstract class Controller {
    /*
        commands is an array with three components:
        - the desired "STEER" (-1 to +1)
        - the desired "THROTTLE" (0 to +1)
        - the deired "BRAKE" (0 to +1)
    */
    public static final int VARIABLE_STEERING = 0;
    public static final int VARIABLE_THROTTLE = 1;
    public static final int VARIABLE_BRAKE = 2;

    /**
     * Threshold values that help determine edge cases for different
     * steering behaviors 
     */
    public static final double ACCELERATION_THRESHOLD = 1;
    public static final double STEERING_THRESHOLD = 0.5;

    /**
     * Values that determine the max acceleration possible for the 
     * controlled vehicle as well as the max speed possible
     */
    public static final int MAX_ACCELERATION = 5;
    public static final int MAX_SPEED = 250;
    
    public abstract void update(Car subject, Game game, double delta_t, double controlVariables[]);

    /**
     * Using a predefined threshold value to allow the car to progress when the 
     * conditions would otherwise yield a situation that doesn't present enough 
     * information for the algorithm to determine a +/- acceleration value
     * (+) values will move the car forward
     * (-) values will reverse the car
     * @param behavior Linear Acceleration 
     * @param controlVariables Car Controls
     */
    public void pedal(double behavior, double[] controlVariables) {
        if (behavior >= -ACCELERATION_THRESHOLD) {
            /** Signal Acceleration */
            controlVariables[VARIABLE_THROTTLE] = 1;
        } else  {
            /** Signal Braking Force */
            controlVariables[VARIABLE_BRAKE] = 1;
        }
    }

    /**
     * Provide the steering input to the vehicle when it is outside of the 
     * provided threshold values. The threshold value is designed to allow the 
     * car to travel in a smooth and straigh line while seeking the target.
     * (+) values will steer the car to the right
     * (-) values will steer the car to the left
     * @param behavior Angular Steering
     * @param controlVariables Car Controls
     */
    public void steer(double behavior, double[] controlVariables) {
        if (behavior > STEERING_THRESHOLD) {
            /** Signal a Right Turn */
            controlVariables[VARIABLE_STEERING] = 1;
        } else if (behavior < -STEERING_THRESHOLD) {
            /** Signal a Left Turn */
            controlVariables[VARIABLE_STEERING] = -1;
        }
    }

    /**
     * Handles updating the vehicles direction according to its heading and 
     * orthogonal angles, as well as the desired vector (A).
     * @param subject Vehicle being controlled
     * @param A Desired Movement Vector
     * @param controlVariables Car Controls
     */
    public void outputFiltering(Car subject, Vector A, double[] controlVariables) {
        /**
         * Default control variables at the beginning of each loop
         * to minimize the number of paths that have to be derived
         * later on
         */
        controlVariables[VARIABLE_STEERING] = 0;
        controlVariables[VARIABLE_THROTTLE] = 0;
        controlVariables[VARIABLE_BRAKE] = 0;

        /**
         * Get the vector for the subject's current trajectory
         * H = <currX, currY> and the orthogonal vector
         * Heading => Acceleration Behavior
         * Rotated => Steering Behavior
         */
        Vector accelerationControl = Vector.fromAngle(subject.getAngle());
        Vector steeringControl = Vector.fromAngle(subject.getAngle() + (Math.PI/2));

        /**
         * Cross the vector towards the target with the current trajectory
         * to determine the direction of the acceleration
         */
        double accelerationBehavior = accelerationControl.dotprod(A);

        /**
         * Cross the vector towards the target with the 90 degress offset
         * to determine which direction the subject should turn
         */
        double steeringBehavior = steeringControl.dotprod(A);

        /**
         * Manipulate the acceleration and steering commands
         */
        pedal(accelerationBehavior, controlVariables);
        steer(steeringBehavior, controlVariables);
    }
}
