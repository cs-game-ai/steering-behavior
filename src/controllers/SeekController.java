package controllers;

import engine.Car;
import engine.Game;
import engine.GameObject;
import engine.Vector;

public class SeekController extends Controller {

    public GameObject m_target;
    
    public SeekController(GameObject target) {
        m_target = target;
    }

    public Vector seek(Car subject) {
        /**
         * Perform the Seek Algorithm to capture the appropriate
         * vector to use for following the target
         */
        
        /**
         * Using the distance formula to calculate the magnitude 
         * of the vector which points towards the target 
         * D = <distX, distY> 
         */
        Vector E = new Vector(this.m_target.getX(), this.m_target.getY());
        Vector CarPosition = new Vector(subject.getX(), subject.getY());
        Vector D = E.subtract(CarPosition);

        /**
         * Normalize the vector using the directional vector 
         * and magnitude (Unit Vector)
         * |V| = <normX, normY>
         */
        Vector ND = D.normalize();

        /**
         * Scale the vector to match the max acceleration of
         * the subject car
         */
        Vector A = ND.scale(MAX_ACCELERATION);

        return A;
    }

    @Override
    public void update(Car subject, Game game, double delta_t, double controlVariables[]) {
        Vector A = seek(subject);
        outputFiltering(subject, A, controlVariables);
    }
}
