package controllers;

import engine.Car;
import engine.Game;
import engine.GameObject;
import engine.Vector;

public class ArriveController extends Controller {

    public GameObject m_target;

    public ArriveController(GameObject marker) {
        m_target = marker;
    }

    /**
     * Perform the Arrive Algorithm to properly arrive at a marker
     * @param subject Vehicle being controlled
     * @param delta_t Time Difference
     * @param targetRadius Radius of the Marker
     * @param slowRadius Radius of the "Speed Limit" Zone
     * @return Desired Vector
     */
    public Vector arrive(Car subject, double delta_t, double targetRadius, double slowRadius) {
        /** 
         * Capture the Marker (E) and Car's positions 
         */
        Vector E = new Vector(this.m_target.getX(), this.m_target.getY());
        Vector CarPosition = new Vector(subject.getX(), subject.getY());

        /**
         * Calculate the distance between the Market and the Car 
         * Note: This is the same thing as the magnitude of the vector
         */
        Vector D = E.subtract(CarPosition);
        double distance = D.magnitude();

        /**
         * When the car enters the target radius, it should stop -- the blank
         * vector will remove the stimulus that activates the steering and petal
         */
        if (distance < targetRadius) {
            return new Vector();
        }

        /**
         * Determine the action of the Car when it is approaching the 
         * Marker as well as when it is beyond the Marker
         */
        double targetSpeed = 0.0;
        if (distance > slowRadius) {
            /** Full Speed Ahead! */
            targetSpeed = MAX_SPEED;
        } else {
            /** Apply the break to slow down the Car */
            targetSpeed = MAX_SPEED * (distance / slowRadius);
        }

        /**
         * Calculate the Target Velocity by normalizing the Distance
         * Vector and scaling it by the Target Speed
         */
        Vector TargetVelocity = D.normalize().scale(targetSpeed);

        /**
         * Capture the Car's current velocity from it's angle vector
         */
        Vector CarVelocity = Vector.fromAngle(subject.getAngle())
            .normalize()
            .scale(subject.getSpeed());

        /**
         * Calculate the return vector by subtracting the Car's current 
         * velocity from the Target velocity and scaling it by the timing
         * factor
         */
        Vector A = TargetVelocity
            .subtract(CarVelocity)
            .scale((1/delta_t));


        if (A.magnitude() > MAX_ACCELERATION) {
            return A.normalize().scale(MAX_ACCELERATION);
        }

        return A;
    }

    @Override
    public void update(Car subject, Game game, double delta_t, double controlVariables[]) {
        Vector A = arrive(subject, delta_t, 0, 100);
        outputFiltering(subject, A, controlVariables);
    }
}
